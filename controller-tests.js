var fs = require('fs');
const util = require('util');
const access = util.promisify(fs.access);
const Family = require('./controller');

const newFamily = new Family('./family.json')

const jsonFileExistTest = async () => {
    await newFamily._createJson()
    const path = './family.json'
    await access(path, fs.F_OK).catch((err) => console.error("jsonFileExistTest is failed. Error: " + err))
    console.log("jsonFileExistTest is done")
}

const validateJsonTest = async () => {
    await newFamily._readFamily();
    if (newFamily.familyObj.constructor === !Object) { throw new Error("validateTest failed! JSON doecn't contain an object ") }

    await newFamily._validateJson()
    const validate = (Array.isArray(newFamily.familyObj.familyMembers) && Array.isArray(newFamily.familyObj.familyGroups));
    if (validate) { console.log('validateTest is done!') } else {
        throw new Error("validateTest failed! Some problem with json structure!");
    }
}

const createMembTest = async () => {
    //memb which posted to json
    // isFirstUser
    const isFirstUser = await newFamily.familyObj.familyMembers.length <= 0;
    const memb = await newFamily.createMember(
        {
            firstName: "scasda",
            lastName: "Tarasascascov",
            group: "parents"
        }
    );
    //memb which received from json
    const newMember = newFamily.familyObj.familyMembers.find(item => item.firstName === memb.firstName && memb.lastName === memb.lastName);
    //first+last name test
    if (newMember.firstName !== memb.firstName && newMember.lastName !== memb.lastName) { throw new Error("createMembTest failed! Some problem with member's firstname or lastname") }
    //group is declarated group
    const findGroup = newFamily.familyObj.familyGroups.find(item => item.id === newMember.group)
    if (findGroup.id !== memb.group) { throw new Error("createMembTest failed! Some problem with member's group") }

    //owner is declarated owner
    if (isFirstUser && memb.owner !== undefined) { throw new Error("createMembTest failed! First member shouldn't has owner") }
    if (!isFirstUser) {
        const findOwner = newFamily.familyObj.familyMembers.find(item => item.firstName === memb.owner.firstName && item.lastName === memb.owner.lastName);
        if (findOwner.firstName !== memb.owner.firstName && findOwner.lastName !== memb.owner.lastName) { throw new Error("createMembTest failed!  Some problem with member's owner") }
    }
    //favourites is declarated favourites
    if (isFirstUser && memb.favourite !== undefined) { throw new Error("createMembTest failed! First member shouldn't has favourite") }
    if (!isFirstUser) {
        favInArray = (array) => {
            array.map(oneFav => {
                const findFav = newFamily.familyObj.familyMembers.find(item => item.firstName === oneFav.firstName && item.lastName === oneFav.lastName);
                if (findFav.firstName !== oneFav.firstName && findFav.lastName !== oneFav.lastName) { throw new Error("createMembTest failed! Some problem with member's favourites") }
            })
        }
        favInArray(memb.favourite)
    }
    //id is ok
    if ((/[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/).test(newMember.id) !== true) { throw new Error("createMembTest failed! Some problem with member's id") }
    console.log("createMembTest is done!")
}

const testInit = async () => {
    await jsonFileExistTest();
    await validateJsonTest();
    await createMembTest();
}
testInit()
'use strict';
const uuidv1 = require('uuid/v1');
var fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const exists = util.promisify(fs.exists);

class Family {
    familyStr = {
        familyMembers: [],
        familyGroups: [
            {
                group: "parents",
                id: 'parentGroupID'
            },
            {
                group: "children",
                id: 'childGroupID'
            },
            {
                group: "pets",
                id: 'petGroupID'
            }
        ]
    }

    familyObj = {}

    async _createJson() {
        const isExist = await exists('family.json');
        if (isExist) {
            console.log("The file exists.");
            await this._readFamily()
            this._validateJson()
        } else {
            console.log('The file does not exist.');
            const json = JSON.stringify(this.familyStr, null, 2)
            await writeFile('family.json', json).catch((e) => console.log(e))
            await this._readFamily()
            console.log('JSON created')
        }
    }

    _validateJson() {
        const validate = (Array.isArray(this.familyObj.familyMembers) && Array.isArray(this.familyObj.familyGroups));
        if (validate) { console.log('Family has been validated!') } else {
            throw new Error("Bad structure of Json!");
        }
    }

    async _readFamily() {
        let rawdata = await readFile('family.json');
        this.familyObj = JSON.parse(rawdata)
    }

    async _addMember(newMember) {
        const familyArray = this.familyObj.familyMembers;
        familyArray.push(newMember);
        const json = JSON.stringify(this.familyObj, null, 2);
        await writeFile('family.json', json).catch((e) => console.log(e));
        await this._readFamily();
        console.log('Family has been updated!');
    }

    _findMember(data) {
        const findObj = this.familyObj.familyMembers.find(item => item.firstName === data.firstName && item.lastName === data.lastName);
        if (findObj === undefined) {
            throw new Error("Member not found!");
        } else { return findObj.id }
    }

    _findGroup(group) {
        const findGroup = this.familyObj.familyGroups.find(item => item.group === group)
        if (findGroup === undefined) {
            throw new Error("Group not found!");
        } else { return findGroup.id }
    }

    _getEveryMember(favourite) { return favourite.map(this._findMember.bind(this)) }

    _checkMembStr(obj) {
        const props = [ "firstName", "lastName", "group", "owner", "favourite" ];
    const hasAll = props.every(prop => obj.hasOwnProperty(prop));
    return hasAll
}

    async createMember({ firstName, lastName, group, owner, favourite }) {
            await newFamily._createJson()
            const isFirstUser = this.familyObj.familyMembers.length <= 0;
            const firstMemberStr = isFirstUser && firstName && lastName && group && !owner && !favourite;
            const nextMembersStr = !isFirstUser && firstName && lastName && group && owner && favourite;
            if (firstMemberStr || nextMembersStr)
            {
            const member = {
                "id": uuidv1(),
                "firstName": firstName,
                "lastName": lastName,
                "group": this._findGroup(group),
                "owner": !isFirstUser && this._findMember(owner),
                "favourite": !isFirstUser && this._getEveryMember(favourite),
            }
            if(!this._checkMembStr(member)) throw new Error("Bad structure of Member!");
            firstMemberStr && delete member["owner"],delete member["favourite"] ;
            console.log('Member has been created');
            this._addMember(member)
            var {firstName, lastName, group, owner, favourite} = member;
            return {
                firstName,
                lastName,
                group,
                owner,
                favourite,
            }
        } else {
            const message = !isFirstUser ? "Bad structure of Member!" : "First member shouldn't has owner or favourite"
             throw new Error(message); 
            }
    }
}

const newFamily = new Family('./family.json')

// newFamily.createMember(
//     { firstName: "Anton", lastName: "Petrof",
//     group: "parents",
//     owner: {firstName: "Anton", lastName: "Petrof"},
//     favourite: [{firstName: "Anton", lastName: "Petrof"}]
// }
// );


module.exports = Family;
var fs = require('fs');
const util = require('util');

const rename = util.promisify(fs.rename)
const unlink = util.promisify(fs.unlink)
const mkdir = util.promisify(fs.mkdir);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

    const markup = `<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <title>The HTML5 Herald</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="SitePoint">
    <link rel="stylesheet" href="#">
  </head>
  <body>
    <script src="js/scripts.js"></script>
  </body>
  </html>`

const addList = async () => {
    const fileReader = await readFile('./src/html/index.html', "utf8").catch((e) => console.log(e))
    const addedList = fileReader.replace(`<body>`,
        `<body>
            <ul style="list-style-type:square;">
                    <li>Coffee</li>
                    <li>Tea</li>
                    <li>Milk</li>
            </ul>`);
    await writeFile('./src/html/index.html', addedList).catch((e) => console.log(e))
    console.log('Item list has been added!')
}

async function fileHandler() {
    await writeFile('example.txt', "Hello world!").catch(e => {throw e});
    console.log('The file has been saved!');
    const data = await readFile('example.txt', 'utf8').catch(e => {throw e});
    console.log(data)
    await rename('example.txt', 'new-example.txt').catch(e => {throw e})
    console.log('Rename complete!');
    await unlink('new-example.txt').catch((e) => console.log(e))
    console.log('File deleted successfully!')
    await mkdir('./src/html/', { recursive: true }).catch((e) => console.log(e));
    await writeFile('./src/html/index.html', markup).catch(e => { throw e });
    console.log('Folders with html have been added!');
    await addList()
    await mkdir('./build/html/', { recursive: true }).catch((e) => console.log(e));
    console.log("build folder has been created")
    await rename('./src/html/index.html', './build/html/index.html')
    console.log("transfer has been done")
}
fileHandler()

